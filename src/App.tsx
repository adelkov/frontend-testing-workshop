import { useState, useEffect } from "react";
import Post from "./types/Post";

function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [posts, setPosts] = useState<Post[]>();

  useEffect(() => {
    function fetchPosts() {
      window
        .fetch("https://jsonplaceholder.typicode.com/posts")
        .then((response) => response.json())
        .then((json) => {
          setPosts(json);
          setIsLoading(false);
        });
    }

    fetchPosts();
  }, []);

  return (
    <div>
      {isLoading && <div>Loading...</div>}
      {posts && (
        <ul>
          {posts.map((post) => (
            <li key={post.id}>{post.title}</li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default App;
